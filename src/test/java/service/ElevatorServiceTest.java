package service;

import library.Direction;
import model.Elevator;
import model.Person;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import static org.testng.Assert.*;
import java.util.LinkedList;
import java.util.List;


public class ElevatorServiceTest {
    ElevatorService elevatorService = new ElevatorService();

    @DataProvider(name = "data")
    public Object[][] createData() {
        Person person1 = new Person(9, Direction.DOWN);
        Person person2 = new Person(10,Direction.UP);
        Person person3 = new Person(12,Direction.DOWN);
        Person person4 = new Person(1,Direction.UP);


        Elevator elevator1 = new Elevator("1",12, 10);
        Elevator elevator2 = new Elevator( "2",8, 9);
        Elevator elevator3 = new Elevator( "3",7, 10);
        Elevator elevator4 = new Elevator( "4",1, 12);
        Elevator elevator5 = new Elevator( "5",12, 1);
        Elevator elevator6 = new Elevator( "6",1, 1);

        List<Elevator> elevators1 = new LinkedList<>();
        elevators1.add(elevator1);
        elevators1.add(elevator2);

        List<Elevator> elevators2 = new LinkedList<>();
        elevators2.add(elevator1);
        elevators2.add(elevator3);

        List<Elevator> elevators3 = new LinkedList<>();
        elevators3.add(elevator4);
        elevators3.add(elevator5);

        List<Elevator> elevators4 = new LinkedList<>();
        elevators4.add(elevator6);
        elevators4.add(elevator5);

        return new Object[][]{{person1, elevators1, elevator2},
                {person2, elevators2, elevator1},
                {person3, elevators3, elevator5},
                {person3, elevators4, elevator5},
                {person4, elevators4, elevator6}};
    }
    @Test(dataProvider = "data")
    public void testNearest(Person person, List<Elevator> elevators, Elevator expected) {
        Elevator actual = elevatorService.chooseElevator(elevators, person);
        assertEquals(actual, expected);
    }

}

package service;

import model.Elevator;
import model.Person;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;

public class ElevatorService {

    public Elevator chooseElevator(List<Elevator> elevators, Person person) {
        List<Elevator> elevatorsWay = new ArrayList<>();

        for (Elevator elevator : elevators) {
            if (elevatorDirection(elevator).equals(person.getPersonDirection().toString()) && isOnTheWay(elevator, person)) {
                elevator.setDistance(Math.abs(elevator.getFloorFrom() - person.getFloor()));
            } else {
                elevator.setDistance(Math.abs(elevator.getFloorTo() - elevator.getFloorFrom())
                        + Math.abs(elevator.getFloorTo() - person.getFloor()));
            }
            elevatorsWay.add(elevator);
        }
        return elevatorsWay.stream().min(Comparator.comparing(Elevator::getDistance)).orElseThrow(NoSuchElementException::new);
    }

    public boolean isOnTheWay(Elevator elevator, Person person) {
        return (elevator.getFloorFrom() >= person.getFloor() && elevatorDirection(elevator).equals("DOWN"))
                || (elevator.getFloorFrom() <= person.getFloor() && elevatorDirection(elevator).equals("UP"));
    }

    public String elevatorDirection(Elevator elevator) {
        String result;
        int elevatorDirection = Integer.compare(elevator.getFloorFrom() - elevator.getFloorTo(), 0);
        result = switch (elevatorDirection) {
            case 1 -> "DOWN";
            case -1 -> "UP";
            default -> "HOLD";
        };
        return result;
    }
}

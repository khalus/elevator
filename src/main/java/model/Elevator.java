package model;

public class Elevator {
    private final int floorFrom;
    private final int floorTo;
    private final String name;
    private  int distance = 0;
    public Elevator(String name, int floorFrom, int floorTo) {
        this.name = name;
        this.floorFrom = floorFrom;
        this.floorTo = floorTo;
    }

    @Override
    public String toString() {
        return "Elevator{" +
                "name='" + name + '\'' +
                ", distance=" + distance +
                '}';
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance=distance;
    }

    public int getFloorFrom() {
        return floorFrom;
    }

    public int getFloorTo() {
        return floorTo;
    }

}

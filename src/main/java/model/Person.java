package model;

import library.Direction;

public class Person {
    private final int floor;
    private final Direction personDirection;

    public Person(int floor, Direction personDirection) {
        this.floor = floor;
        this.personDirection = personDirection;
    }

    public int getFloor() {
        return floor;
    }

    public Direction getPersonDirection() {
        return personDirection;
    }

}

package main;

import library.Direction;
import model.Elevator;
import model.Person;
import service.ElevatorService;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ElevatorService elevatorService=new ElevatorService();
        Person person3 = new Person(12, Direction.DOWN);
        Elevator elevator4 = new Elevator( "4",1, 12);
        Elevator elevator5 = new Elevator( "5",12, 1);
        Elevator elevator6 = new Elevator( "6",1, 1);
        List<Elevator> elList = new ArrayList<>();
        elList.add(elevator4);
        elList.add(elevator5);
        elList.add(elevator6);
        elevatorService.chooseElevator(elList,person3);
    }
}
